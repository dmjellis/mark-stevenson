<?php get_header(); ?>

<div id="main-content" class="wrap">

	<div class="container">

		<div id="content" class="twelve columns">
			
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						
					<h1 class="main-header"><?php the_title(); ?></h1>
					
					<p class="post-date"><span><?php the_time('jS F Y') ?></span></p>
					
					<?php if ( has_post_thumbnail() ) {
						the_post_thumbnail('original',  array('class' => 'project-featured-image')); 
						} else {
						echo '<img src="';
						echo bloginfo('template_directory'); 
						echo '/images/default-slide.jpg " alt="" />';
						}
					?>
					
					<div class="post-content">
						<?php the_content();?>
					</div>
					
					<hr/>
					<div class="postmetadata">
					<p>Posted <?php the_time('F jS, Y') ?> in <?php the_category(', ') ?>.</p>
	                <p><?php the_tags(); ?>
					<p>You can follow any responses to this entry through the <?php post_comments_feed_link('RSS 2.0'); ?> feed.
	
						<?php if (('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
							// Both Comments and Pings are open ?>
							You can <a href="#respond">leave a response</a>, or <a href="<?php trackback_url(); ?>" rel="trackback">trackback</a> from your own site.
	
						<?php } elseif (!('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
							// Only Pings are Open ?>
							Responses are currently closed, but you can <a href="<?php trackback_url(); ?> " rel="trackback">trackback</a> from your own site.
	
						<?php } elseif (('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
							// Comments are open, Pings are not ?>
							You can skip to the end and leave a response. Pinging is currently not allowed.
	
						<?php } elseif (!('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
							// Neither Comments, nor Pings are open ?>
							Both comments and pings are currently closed.
	
						<?php } edit_post_link('Edit this entry','','.'); ?>
					</p>
					</div>
						
					<?php comments_template('', true); ?>
					    
					</article>
				        
			<?php endwhile; else: ?>
			
			<p>Sorry, nothing found!</p>
	
			<?php endif; ?>
		
		</div><!-- /content -->
		
		<?php get_sidebar(); ?>
		
	</div><!-- /container -->

</div><!-- /main-content wrap -->

<?php get_footer(); ?>