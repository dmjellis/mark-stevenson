<nav id="primary-nav">
	<ul>
		<li><a href="#front-writer">Writer/Broadcaster</a></li>
		<li><a href="#front-advisory">Advisory</a></li>
		<li><a href="#front-speaking">Public Speaker</a></li>
		<li><a href="#front-business">Business Owner</a></li>
		<li><a href="#front-misc">Media</a></li>
		<li><a href="#front-contact">Contact</a></li>
	</ul>
</nav>
<?php wp_footer(); ?>

</body>
</html>