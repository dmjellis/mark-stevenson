<?php get_header(); ?>

<div id="main-content" class="wrap">

	<div class="container">
	
		<div id="content" class="twelve columns">
	
		<?php while ( have_posts() ) : the_post(); ?>
			
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
				<h1 class="post-title"><?php the_title(); ?></h1>
				
				<?php if ( has_post_thumbnail() ) {
					the_post_thumbnail('original',  array('class' => 'project-featured-image')); 
					} else {
					echo '<img src="';
					echo bloginfo('template_directory'); 
					echo '/images/default-slide.jpg " alt="" />';
					}
				?>
				
				<div class="post-content">
					<?php the_content(); ?>
				</div>
			
			</article>
					
		<?php endwhile; ?>
		
		</div><!-- /content -->
	
		<?php get_sidebar(); ?>
		
	</div><!-- /container -->

</div><!-- /main-content wrap -->
	
<?php get_footer(); ?>