<?php get_header(); ?>

<div class="wrap">
	
	<div class="container">
	
		<div id="fourohfour" class="twelve columns offset-by-two">
		
			<h1>Nothing Found</h1>
			<p>Apologies, but the page you requested could not be found. Perhaps searching will help.</p>
			<?php get_search_form(); ?>
		
			<script type="text/javascript">
				// focus on search field after it has loaded
				document.getElementById('s') && document.getElementById('s').focus();
			</script>
		
		</div>
	
	</div>
</div>
	
<?php get_footer(); ?>