<?php get_header(); ?>

<div id="main-content" class="wrap">

	<div class="container">

		<div id="content" class="twelve columns index">
			
			<h1 class="main-header">Articles</h1>
			
			<?php while ( have_posts() ) : the_post(); ?>
			
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
					<h2 class="post-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e( "Permalink to", "custom" ); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					
					<p class="post-date"><?php the_time('jS M Y') ?></p>
					
					<div class="post-content">
						<?php the_excerpt(); ?>
					</div>
				
				</article>
	
			<?php endwhile; ?>
				
			<div id="custom-pagenavi">
				<?php if ( function_exists( 'molly_pagenavi' ) ) { ?>
					<ul class="pagination">
						<?php molly_pagenavi(); ?>
					</ul>
				<?php } else { ?>
					<div class="alignleft">
						<?php next_posts_link( __( '&laquo; Older entries', "custom" ) ); ?>
					</div>
					<div class="alignright">
						<?php previous_posts_link( __( 'Newer entries &raquo;', "custom" ) ); ?>
					</div>
				<?php } ?>
			</div>
	
		</div><!-- /content -->

		<?php get_sidebar(); ?>
		
	</div><!-- /container -->

</div><!-- /main-content wrap -->

<?php get_footer(); ?>