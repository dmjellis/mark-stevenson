<?php
// Text domain
load_theme_textdomain( 'custom', TEMPLATEPATH . '/languages' );
$locale = get_locale();
$locale_file = TEMPLATEPATH . "/languages/$locale.php";
if ( is_readable($locale_file) )
	require_once($locale_file);
	
// Load Javascript
function molly_load_scripts() {
	if (!is_admin()) {
		wp_deregister_script( 'jquery' );  
    	// Register the library again from Google's CDN  
    	wp_register_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', array(), null, false );  
		wp_enqueue_script('jquery'); 
		// Register our Javascript
		wp_register_script('flexslider', get_template_directory_uri() . '/js/jquery.flexslider-min.js');
		wp_register_script('meanmenu', get_template_directory_uri() . '/js/jquery.meanmenu.min.js');
		wp_register_script('app', get_template_directory_uri() . '/js/app.js');
		// Load our Javascript
		wp_enqueue_script('flexslider');
		wp_enqueue_script('meanmenu');
		wp_enqueue_script('app');
	}
}
add_action('wp_enqueue_scripts', 'molly_load_scripts');

// Register widgets/sidebars
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name'=> 'Sidebar',
		'id' => 'sidebar',
		'before_widget' => '<div class="widget">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
}
// Enable menus
add_theme_support( 'nav-menus' );
add_action( 'init', 'register_my_menus' );
function register_my_menus() {
	register_nav_menus(
		array(
			'primary' => __( 'Primary' ),
			'footer' => __( 'Footer' )
		)
	);
}
// Feed support 
add_theme_support('automatic-feed-links');
// Disable the admin bar, set to true if you want it to be visible.
show_admin_bar(FALSE);
// Post thumbnails 
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 60, 60, true ); // hard crop mode
add_image_size( 'advisory-logo', 300, 186, true );
add_image_size( 'book-cover', 300, 465, true );
add_image_size( 'small-cover', 90, 140, true );
// http://css-tricks.com/snippets/wordpress/remove-width-and-height-attributes-from-inserted-images/
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );
function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}
// Check browser (and stuff it into our body class)
add_filter('body_class','molly_browser_body_class');
function molly_browser_body_class($classes) {
	global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;	
	if($is_lynx) $classes[] = 'lynx';
	elseif($is_gecko) $classes[] = 'gecko';
	elseif($is_opera) $classes[] = 'opera';
	elseif($is_NS4) $classes[] = 'ns4';
	elseif($is_safari) $classes[] = 'safari';
	elseif($is_chrome) $classes[] = 'chrome';
	elseif($is_IE) $classes[] = 'ie';
	else $classes[] = 'unknown';

	if($is_iphone) $classes[] = 'iphone';
	return $classes;
}
// Remove WordPress "junk" from the header file (remove lines if it's something here that you want to use)
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
// Remove rel attribute from the category list, for validation
function remove_category_list_rel($output)
{
  $output = str_replace(' rel="category tag"', '', $output);
  return $output;
}
add_filter('wp_list_categories', 'remove_category_list_rel');
add_filter('the_category', 'remove_category_list_rel');
// Custom page navigation
function molly_pagenavi( $p = 2 ) { // pages will be show before and after current page
	if ( is_singular() ) return; // don't show in single page
	global $wp_query, $paged;
	$max_page = $wp_query->max_num_pages;
	if ( $max_page == 1 ) return; // don't show when only one page
	if ( empty( $paged ) ) $paged = 1;
	//echo '<span class="pages">Page: ' . $paged . ' of ' . $max_page . ' </span> '; // pages
	if ( $paged > $p + 1 ) p_link( 1, 'First' );
	if ( $paged > $p + 2 ) echo '<li class="unavailable"><a href="#">&hellip;</a></li>';
	for( $i = $paged - $p; $i <= $paged + $p; $i++ ) { // Middle pages
		if ( $i > 0 && $i <= $max_page ) $i == $paged ? print "<li class='current'><a href='#'>{$i}</a></li> " : p_link( $i );
	}
	if ( $paged < $max_page - $p - 1 ) echo '<li class="unavailable"><a href="#">&hellip;</a></li>';
	if ( $paged < $max_page - $p ) p_link( $max_page, 'Last' );
}
function p_link( $i, $title = '' ) {
	if ( $title == '' ) $title = "Page {$i}";
	echo "<li><a href='", esc_html( get_pagenum_link( $i ) ), "' title='{$title}'>{$i}</a></li> ";
}
// Comments
// Custom callback to list comments
function custom_comments($comment, $args, $depth) {
  $GLOBALS['comment'] = $comment;
    $GLOBALS['comment_depth'] = $depth;
  ?>
    <li id="comment-<?php comment_ID() ?>" <?php comment_class() ?>>
        <div class="comment-author vcard"><?php commenter_link() ?></div>
        
  <?php if ($comment->comment_approved == '0') _e("\t\t\t\t\t<span class='unapproved'>Your comment is awaiting moderation.</span>\n", 'molly') ?>
          <div class="comment-content">
            <?php comment_text() ?>
        </div>
        <div class="comment-meta">
        <?php printf(__('%1$s <span class="meta-sep">|</span> <a href="%3$s" title="Permalink to this comment">Permalink</a>', 'molly'),
                    get_comment_date(),
                    get_comment_time(),
                    '#comment-' . get_comment_ID() );
                    edit_comment_link(__('Edit', 'molly'), ' <span class="meta-sep">|</span> <span class="edit-link">', '</span>'); 
                    if($args['type'] == 'all' || get_comment_type() == 'comment') :
                comment_reply_link(array_merge($args, array(
                    'reply_text' => __('Reply to this comment','molly'),
                    'login_text' => __('Log in to reply.','molly'),
                    'depth' => $depth,
                    'before' => ' <span class="meta-sep">|</span> <span class="comment-reply-link">',
                    'after' => '</span>'
                )));
            endif;
         ?></div>
<?php } // end custom_comments
// Custom callback to list pings
function custom_pings($comment, $args, $depth) {
       $GLOBALS['comment'] = $comment;
        ?>
            <li id="comment-<?php comment_ID() ?>" <?php comment_class() ?>>
                <div class="comment-author"><?php printf(__('By %1$s on %2$s at %3$s', 'molly'),
                        get_comment_author_link(),
                        get_comment_date(),
                        get_comment_time() );
                        edit_comment_link(__('Edit', 'molly'), ' <span class="meta-sep">|</span> <span class="edit-link">', '</span>'); ?></div>
    <?php if ($comment->comment_approved == '0') _e('\t\t\t\t\t<span class="unapproved">Your trackback is awaiting moderation.</span>\n', 'molly') ?>
            <div class="comment-content">
                <?php comment_text() ?>
            </div>
<?php } // end custom_pings
// Produces an avatar image with the hCard-compliant photo class
function commenter_link() {
    $commenter = get_comment_author_link();
    if ( ereg( '<a[^>]* class=[^>]+>', $commenter ) ) {
        $commenter = ereg_replace( '(<a[^>]* class=[\'"]?)', '\\1url ' , $commenter );
    } else {
        $commenter = ereg_replace( '(<a )/', '\\1class="url "' , $commenter );
    }
    $avatar_email = get_comment_author_email();
    $avatar = str_replace( "class='avatar", "class='photo avatar", get_avatar( $avatar_email, 60 ) );
    echo $avatar . ' <span class="fn n">' . $commenter . '</span>';
}