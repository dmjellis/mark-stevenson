<?php get_header(); ?>

<div id="main-content" class="wrap">

	<div class="container">
		
		<div id="content" class="twelve columns">
				
			<h1>Search Results</h1> 
			
			<p><?php $s = $wp_query->query_vars['s'];
			echo $wp_query->found_posts . " results found with '$s'" ?></p>
		
			<?php if (have_posts()) : while ( have_posts() ) : the_post(); ?>
			
			
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
				<h2 class="post-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e( "Permalink to", "custom" ); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				
				<aside class="meta-content">
				</aside>
				
				<div class="post-content">
					<?php the_excerpt(); ?>
				</div>
			
			</article>
			
			<?php endwhile; ?>
				
			<div id="custom-pagenavi">
				<?php if ( function_exists( 'nyan_pagenavi' ) ) { ?>
					<ul class="pagination">
						<?php nyan_pagenavi(); ?>
					</ul>
				<?php } else if ( function_exists( 'wp_pagenavi' ) ) { wp_pagenavi(); } else { ?>
					<div class="alignleft">
						<?php next_posts_link( __( '&laquo; Older entries', "custom" ) ); ?>
					</div>
					<div class="alignright">
						<?php previous_posts_link( __( 'Newer entries &raquo;', "custom" ) ); ?>
					</div>
				<?php } ?>
			</div>
			
			<?php else : ?>
			
			<p>Sorry, nothing found for that search term.</p>
			
			<?php endif; ?>
			
		</div><!-- /content -->

		<?php get_sidebar(); ?>
		
	</div><!-- /container -->

</div><!-- /main-content wrap -->
		
<?php get_footer(); ?>