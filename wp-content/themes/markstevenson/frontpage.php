<?php
/*
Template Name: Frontpage
*/
?>
<?php get_header(); ?>

<section id="front-strapline" class="wrap">

	<div class="container">
		<?php if(get_field('strapline')){ ?>
			<h1 id="intro-strapline" class="center"><?php the_field('strapline'); ?></h1>
		<?php } ?>
	</div>

</section>

<section id="front-intro" class="wrap" style="background: url(<?php the_field('intro_image'); ?>) no-repeat top center">

	<div class="container">
		
		<?php if(get_field('quote')){ ?>
		<blockquote>
  		<p><?php the_field('quote'); ?></p>
  		<cite><?php the_field('citation'); ?></cite>
  		</blockquote>
		<?php } ?>
		
	</div>
	
</section>

<section id="front-writer" class="wrap">
	
	<div class="container">
	
		<div class="ten columns">
			
			<?php if(get_field('writer_section_title')){ ?>
				<h2 class="section-title"><?php the_field('writer_section_title'); ?></h2>
			<?php } ?>
			
			<?php if(get_field('writer_text')){ ?>
				<div id="writer-text">
				<?php the_field('writer_text'); ?>
				</div>
			<? } ?>
		</div>
		
		<div class="five columns offset-by-one">
			
			<?php if(get_field('book_section_title')){ ?>
				<h2 class="section-title"><?php the_field('book_section_title'); ?></h2>
			<?php } ?>
			
				<div class="flexslider">
					<?php if(get_field('book')): ?>
					<ul class="slides">
					<?php while(has_sub_field('book')): ?>
					
		  	    	   		<?php 
		  	    	   		$attachment_id = get_sub_field('cover_image');
							$size = "book-cover"; 
							$image = wp_get_attachment_image_src( $attachment_id, $size ); 
							$thumb_size = "small-cover"; 
							$thumb_image = wp_get_attachment_image_src( $attachment_id, $thumb_size ); ?>
							<li class="slide" data-thumb="<?php echo $thumb_image[0]; ?>">
							<a href="<?php the_sub_field('book_link'); ?>">
							<img class="slide-image" src="<?php echo $image[0]; ?>" alt="" /> </a>
		  	    		</li>
		  	    		
		  	    	<?php endwhile; ?>
		          	</ul>
		          	<?php endif; ?>
				</div><!-- /flexslider -->
		</div>
		
		<div class="twelve columns offset-by-two">
		
			<?php if(get_field('writer_quotes')): ?>
			<?php while(has_sub_field('writer_quotes')): ?>
				<blockquote>
		  		<?php the_sub_field('writer_quote'); ?>
		  		<cite><?php the_sub_field('writer_citation'); ?></cite>
		  		</blockquote>
	  		<?php endwhile; ?>
       	 	<?php endif; ?>
		</div>
		
	</div>
	
</section>

<section id="front-twitter" class="wrap">

	<div class="container">
		
		<div id="display-tweet">
			<div class="two columns alpha offset-by-one">
				<img src="<?php bloginfo('template_directory'); ?>/images/twitter-white.png" alt="Twitter" />
			</div>
			<div class="twelve columns">
			<?php if(get_field('twitter_section_title')){ ?>
				<h2 class="section-title"><?php the_field('twitter_section_title'); ?></h2>
			<?php } ?>
				<?php if ( function_exists( "display_tweets" ) ) { display_tweets(); } ?>
				<div id="follow-button">
				<a href="https://twitter.com/Optimistontour" class="twitter-follow-button" data-show-count="false" data-size="large">Follow @Optimistontour</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
				</div>
			</div>
		</div>
		
	</div>
	
	<img id="twitter-top" src="<?php bloginfo('template_directory'); ?>/images/business-top.png" alt="" />
	<img id="twitter-bottom" src="<?php bloginfo('template_directory'); ?>/images/twitter-bottom.png" alt="" />

</section>

<section id="front-advisory" class="wrap">

	<div class="container">
		
		<?php if(get_field('advisory_section_title')){ ?>
		<div class="sixteen columns">
			<h2 class="section-title"><?php the_field('advisory_section_title'); ?></h2>
		</div>
		<?php } ?>
		
		<?php if(get_field('advisory_col1')){ ?>
		<div class="eight columns">
			<div class="advisory-inner">
			<?php the_field('advisory_col1'); ?>
			</div>
		</div>
		<?php } ?>
		
		<?php if(get_field('advisory_col2')){ ?>
		<div class="eight columns">
			<div class="advisory-inner">
			<?php the_field('advisory_col2'); ?>
			</div>
		</div>
		<?php } ?>
		
		<div class="sixteen columns">
		<?php 
		$i = 0;
		if(get_field('company_logo')): ?>
			<?php while(has_sub_field('company_logo')): $i++; ?>
            	<div class="company-logo one-third column<?php if($i==1 || $i%3==1){echo ' alpha';}?><?php if($i%3==0){echo ' omega';}?>">
            		<a href="<?php the_sub_field('url'); ?>" target="_blank">
  	    	   		<?php $attachment_id = get_sub_field('logo_image');
					$size = "advisory-logo"; 
					$image = wp_get_attachment_image_src( $attachment_id, $size ); ?>
					
					<img src="<?php echo $image[0]; ?>" alt="" /> 
					</a>
  	    		</div>
  	    	<?php endwhile; ?>
        <?php endif; ?>
		</div>
		
	</div>
	
</section>

<section id="front-divider" class="wrap">
		<img id="divider-top" src="<?php bloginfo('template_directory'); ?>/images/divider-top.png" alt="" />
		<img id="divider-bottom" src="<?php bloginfo('template_directory'); ?>/images/divider-bottom.png" alt="" />
</section>

<section id="front-speaking" class="wrap">

	<div class="container">
		
		<?php if(get_field('speaking_section_title')){ ?>
		<div class="sixteen columns center">
			<h2 class="section-title"><?php the_field('speaking_section_title'); ?></h2>
		</div>
		<?php } ?>
		
		<?php if(get_field('speaking_text')){ ?>
			<div class="ten columns offset-by-three">
			<?php the_field('speaking_text'); ?>
			</div>
		<? } ?>
		
		<?php if(get_field('speaking_quotes')): ?>
		<?php while(has_sub_field('speaking_quotes')): ?>
			<div class="sixteen columns">
				<blockquote>
		  		<?php the_sub_field('speaking_quote'); ?>
		  		<cite><?php the_sub_field('speaking_citation'); ?></cite>
		  		</blockquote>
	  		</div>
  		<?php endwhile; ?>
   	 	<?php endif; ?>
		
	</div>
	
</section>

<section id="front-business" class="wrap">

	<div class="container">
	
		<div class="sixteen columns">
			<?php if(get_field('business_section_title')){ ?>
				<h2 class="section-title"><?php the_field('business_section_title'); ?></h2>
			<?php } ?>
			<?php if(get_field('business_intro_text')){ ?>
				<div id="business-intro-text"><?php the_field('business_intro_text'); ?></div>
			<?php } ?>
		</div>
		
		<?php if(get_field('business_company')): ?>
		<?php while(has_sub_field('business_company')): ?>
			<div class="eight columns">
				<div class="company-inner">
					<a class="business-logo" href="<?php the_sub_field('company_link'); ?>" target="_blank">
  	    	   		<?php $attachment_id = get_sub_field('company_logo');
					$size = "advisory-logo"; 
					$image = wp_get_attachment_image_src( $attachment_id, $size ); ?>
					
					<img src="<?php echo $image[0]; ?>" alt="" /> 
					</a>
					<h2><?php the_sub_field('business_company_name'); ?></h2>
					<div><?php the_sub_field('business_company_detail'); ?></div>
				</div>
			</div>
		<?php endwhile; ?>
   	 	<?php endif; ?>

	</div>
	
		<img id="business-top" src="<?php bloginfo('template_directory'); ?>/images/business-top.png" alt="" />
		<img id="business-bottom" src="<?php bloginfo('template_directory'); ?>/images/business-bottom.png" alt="" />
	
</section>

<section id="front-misc" class="wrap">

	<div class="container">
		
		<div class="six columns">
			<?php if(get_field('misc_left_column')){ ?>
				<?php the_field('misc_left_column'); ?>
			<?php } ?>
		</div>
		
		<div class="ten columns">
			<?php if(get_field('misc_section_title')){ ?>
				<h2 class="section-title"><?php the_field('misc_section_title'); ?></h2>
			<?php } ?>
			<?php if(get_field('misc_right_column')){ ?>
				<?php the_field('misc_right_column'); ?>
			<?php } ?>
		</div>
		
	</div>
	
</section>

<section id="front-contact" class="wrap center">
	
	<div class="container">

		<div class="sixteen columns">
			<?php if(get_field('contact_section_title')){ ?>
				<h2 class="section-title"><?php the_field('contact_section_title'); ?></h2>
			<?php } ?>
		</div>
		
		<?php if(get_field('contact_person')): ?>
		<?php while(has_sub_field('contact_person')): ?>
			<div class="sixteen columns contact-person">
				<h3><?php the_sub_field('contact_name'); ?></h3>
				<p class="contact-email"><a href="mailto:<?php the_sub_field('contact_email'); ?>"><?php the_sub_field('contact_email'); ?></a></p>
			</div>
		<?php endwhile; ?>
   	 	<?php endif; ?>
		
		<div class="ten columns offset-by-three">
			<?php if(get_field('contact_form_title')){ ?>
				<h2 id="contact-mark" class="section-title"><?php the_field('contact_form_title'); ?></h2>
			<?php } ?>
		
			<?php if(get_field('contact_text')){ ?>
				<?php the_field('contact_text'); ?>
			<?php } ?>
		</div>
				
	</div>
	
	<a id="scroll-top" href="#front-intro"><img src="<?php bloginfo('template_directory'); ?>/images/scroll-top.png" alt="Scroll to top" /></a>
	
</section>

<?php get_footer(); ?>