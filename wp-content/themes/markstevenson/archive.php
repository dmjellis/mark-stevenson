<?php get_header(); ?>

<div id="main-content" class="wrap">

	<div class="container">
	
		<div id="content" class="twelve columns index">
			
			<?php if (have_posts()) : ?>
			
		 	  <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
		 	  <?php if (is_category()) { ?>
				<h1 class="main-header"><?php single_cat_title(); ?> <?php _e( "Category", "custom" ); ?></h1 class="main-header">
		 	  <?php } elseif( is_tag() ) { ?>
				<h1 class="main-header"><?php _e( "Tagged with", "custom" ); ?> <?php single_tag_title(); ?></h1 class="main-header">
		 	  <?php } elseif (is_day()) { ?>
				<h1 class="main-header"><?php the_time('F jS Y'); ?> <?php _e( "Archive", "custom" ); ?></h1 class="main-header">
		 	  <?php  } elseif (is_month()) { ?>
				<h1 class="main-header"><?php the_time('F Y'); ?> <?php _e( "Archive", "custom" ); ?></h1 class="main-header">
		 	  <?php } elseif (is_year()) { ?>
				<h1 class="main-header"><?php the_time('Y'); ?> <?php _e( "Archive", "custom" ); ?> </h1 class="main-header">
			  <?php  } elseif (is_author()) { ?>
				<h1 class="main-header"><?php _e( "Author Archive", "custom" ); ?></h1 class="main-header">
		 	  <?php  } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
				<h1 class="main-header"><?php _e( "Blog Archives", "custom" ); ?></h1 class="main-header">
		 	  <?php } ?>
			
			<?php while ( have_posts() ) : the_post(); ?>
			
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
					<h2 class="post-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e( "Permalink to", "custom" ); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					
					<p class="post-date"><?php the_time('jS M Y') ?></p>
					
					<div class="post-content">
						<?php the_excerpt(); ?>
					</div>
				
				</article>
			
			<?php endwhile; endif; ?>
			
			<div id="custom-pagenavi">
				<?php if ( function_exists( 'molly_pagenavi' ) ) { ?>
					<ul class="pagination">
						<?php molly_pagenavi(); ?>
					</ul>
				<?php } else { ?>
					<div class="alignleft">
						<?php next_posts_link( __( '&laquo; Older entries', "custom" ) ); ?>
					</div>
					<div class="alignright">
						<?php previous_posts_link( __( 'Newer entries &raquo;', "custom" ) ); ?>
					</div>
				<?php } ?>
			</div>
		
		</div><!-- /content -->
	
		<?php get_sidebar(); ?>
		
	</div><!-- /container -->

</div><!-- /main-content wrap -->
	
<?php get_footer(); ?>