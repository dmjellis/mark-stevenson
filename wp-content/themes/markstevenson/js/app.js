$(document).ready(function() {

	$('#primary-nav').meanmenu({
    	meanScreenWidth: "2500"	
    });

   	$('.flexslider').flexslider({
	    animation: "slide",
	    directionNav: false,
	    controlNav: "thumbnails"
	  });
 	 
 	 $("a[href='#front-intro']").click(function() {
	  $("html, body").animate({ scrollTop: 0 }, "slow");
	  return false;
	});

});